
var Ajedrez = (function(){

    var mensaje = document.getElementById("mensaje");

    var _mostrar_tablero = function () {

        var tabla = document.createElement("table");
        var contenedor_tabla = document.getElementById("tablero");
        var tabla = document.createElement("table");
        tabla.setAttribute("width",400);
        tabla.setAttribute("height",400);
        tabla.setAttribute("border",1);
        tabla.setAttribute("cellspacing",2);
        tabla.setAttribute("cellpadding",2);
        tabla.setAttribute("bgcolor","#000000");

        for (let i = 8; i > 0; i--) {
            var columna = document.createElement("tr");
            columna.setAttribute("aling", "center");

            for (let j = 8; j > 0; j--) {
                var celda = document.createElement("td");
                var letra = "";
                switch (j) {
                    case 8:
                        letra = "a";
                        break;
                    case 7:
                        letra = "b";
                        break;
                    case 6:
                        letra = "c";
                        break;
                    case 5:
                        letra = "d";
                        break;
                    case 4:
                        letra = "e";
                    break;
                    case 3:
                        letra = "f";
                    break;
                    case 2:
                        letra = "g";
                    break;
                    case 1:
                        letra = "h";
                    break;

                    default:
                        break;
                }
                 if (i % 2 == 0) {

                    if(j % 2 == 0){
                        celda.setAttribute("bgcolor", "#ffffff");
                        celda.setAttribute("id",letra+i);
                    }else{
                       celda.setAttribute("bgcolor", "#44ffff");
                       celda.setAttribute("id",letra+i);
                   }
                }else{
                   if(j % 2 == 0){
                    celda.setAttribute("bgcolor", "#44ffff");
                    celda.setAttribute("id",letra+i);
                 }else{
                     celda.setAttribute("bgcolor", "#ffffff");
                     celda.setAttribute("id",letra+i);
                }
                }
                celda.setAttribute("align","center");
                celda.setAttribute("width",40);
                celda.setAttribute("height",40);
                columna.appendChild(celda);


            }
            tabla.appendChild(columna);

        }
        contenedor_tabla.appendChild(tabla);


        poner_boton();
        _actualizar_tablero();
    };


    var _actualizar_tablero = function () {

        var servidor = "https://hipolitomartinezoscar.bitbucket.io";
        var url = servidor + "/csv/tablero.csv" + "?r=" + Math.random();

        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
          if (this.readyState === 4) {
            mensaje.textContent = "";
            if (this.status === 200) {
                   colocar_piezas(xhr);

            } else {

                mensaje.textContent = "Error " + this.status + " " +
                this.statusText + " - " + this.responseURL;
            }
          }
        };
        xhr.open('GET', url, true);
        xhr.send();
     };

    var _mover_pieza = function(movimiento){
    try{
        var actual =  document.getElementById(movimiento.de);
       var poner = document.getElementById(movimiento.a);
       var pieza = actual.innerHTML;

       if(actual.innerHTML != "" && poner.innerHTML == ""){
        poner.innerText = pieza;
        actual.innerHTML="";
        mensaje.textContent = "Movimiento de: "+movimiento.de+" - "+"a: "+movimiento.a;
       }else{
        mensaje.textContent = "movimiento invalido";
       }
    }catch(e) {
        console.log("---> "+e);
        mensaje.textContent = "error en el ojeto: "+e;
      }
    };

    var poner_boton = function () {
        var contenedor = document.getElementById("opcion");
        var boton = document.createElement("button");
        boton.innerText = "Botoncito";
        boton.addEventListener("click",_actualizar_tablero);
        contenedor.appendChild(boton);

        };

    return{
        "mostrarTablero": _mostrar_tablero,
        "actualizarTablero": _actualizar_tablero,
        "moverPieza": _mover_pieza
    }

})();


 var colocar_piezas = function (xhr) {

    var content = xhr.responseText;
    var myArray = content.split("\n");
    var indice = 8;

    for (let i=1; i<myArray.length-1; i++) {
            var celda = myArray[i].split('|');

                 for (let j = 0; j < celda.length; j++) {
                    var letra = "";
                    switch (j) {
                        case 7:
                            letra = "a";
                            break;
                        case 6:
                            letra = "b";
                            break;
                        case 5:
                            letra = "c";
                            break;
                        case 4:
                            letra = "d";
                            break;
                        case 3:
                            letra = "e";
                        break;
                        case 2:
                            letra = "f";
                        break;
                        case 1:
                            letra = "g";
                        break;
                        case 0:
                            letra = "h";
                        break;

                        default:
                        mensaje.textContent = "Error pieza no encontrada ";
                            break;
                    }
                    if(myArray.length == 10 && celda.length == 8){
                    switch (celda[j]) {
                        case '∅':
                        document.getElementById(letra+indice).innerHTML = "";
                            break;
                        case "":
                        document.getElementById(letra+indice).innerHTML = "N/A";
                            break;
                        default:
                        document.getElementById(letra+indice).innerHTML = celda[j];
                           break;
                    }
                   }else{
                    console.log(myArray.length+" "+celda.length);
                    mensaje.textContent = "Error las pieza del tablero no son suficientes :'(";
                   }
                    //console.log(i+" "+j+" "+celda[j]+" "+indice);

                 }
                 indice--;
                }

 }
